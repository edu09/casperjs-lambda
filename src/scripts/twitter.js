
// Casperインスタンス生成
var casper = require('casper').create({
  viewportSize: { width: 1024, height: 768 },
  userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36'
});

var sum_log = '';



var arg_arr = casper.cli.args;
var USER_NAME = casper.cli.get("id");
var LAST_POST = casper.cli.get("timestamp_last_post_in_db");
var NUM_POSTS_NEW = casper.cli.get("num_posts");
var NEXT = true;

var ip = casper.cli.get("ip");
var port = casper.cli.get("port");
var protocol = casper.cli.get("protocol");

if (ip !== null && port !== null && protocol !== null ) {
    phantom.setProxy(ip, port, protocol, '', '');
}

// get url api when instagram call api
casper.on("resource.received", function(resource){
    /*if (resource.url.indexOf("query_id") !== -1 && resource.url.indexOf("after") !== -1) {
        URL_API = resource.url;
    }*/
});

casper.start('https://twitter.com/' + USER_NAME + '/');


// ユーザー情報取得
var data = {};
casper.then(function() {
  var elCheckUser = this.evaluate(function() {
    return document.querySelectorAll('.search-404').length;
  });
  if(elCheckUser > 0) {
    dumpEcho(this, {'error': 'username wrong.'});
    //this.exit();
  }
  else {
    data = this.evaluate(function() {
      try {
        var cover = document.querySelector('div.ProfileCanopy-headerBg img').getAttribute('src');
        var birthday = document.querySelector('div.ProfileHeaderCard-birthdate span.ProfileHeaderCard-birthdateText').textContent
          .trim().replace(/Born|on|,| /g, '').replace(/January/g, '01').replace(/February/g, '02').replace(/March/g, '03')
          .replace(/April/g, '04').replace(/May/g, '05').replace(/June/g, '06').replace(/July/g, '07').replace(/August/g, '08')
          .replace(/September/g, '09').replace(/October/g, '10').replace(/November/g, '11').replace(/December/g, '12');

        var y = birthday.slice(-4);
        var m = birthday.slice(0, 2);
        var d = birthday.replace(y, '').replace(m, '');
        if(d.length === 1) {
          d = '0' + d;
        }

        birthday = y + m + d;
        if(birthday.length !== 8) {
          birthday = '';
        }

        var follower = document.querySelector('li.ProfileNav-item--followers span.ProfileNav-value');

        return {
          'cover'    : cover === null || cover.length === 0 ? '' : cover,
          'avatar'   : document.querySelector('img.ProfileAvatar-image').getAttribute('src').replace(/_400x400/g, ''),
          'name'     : document.querySelector('h1.ProfileHeaderCard-name > a').textContent,
          'followers': follower === null ? '0' : follower.textContent,
          'birthday' : birthday,
          'address'  : document.querySelector('div.ProfileHeaderCard-location span.ProfileHeaderCard-locationText').textContent.trim(),
          'introduce': document.querySelector('h2.ProfileHeaderCard-screenname + p').textContent.trim(),
          'posts'    : []
        };
      } catch(err) {
        casper.log(err.name + ':' + err.message + '\n' + err.stack,"info")
      }
    });

    data['followers'] = convertNum(data['followers']);
    data['user_name'] = USER_NAME;
  }
});

casper.then(function () {
    var checkNext = checkHasNewPosts(this);
    loopScroll(this);
});


// 各postに振られているidを取得
var post_ids = [];
casper.wait(1000, function() {
  post_ids = this.evaluate(function(user_name) {
    var arr = [];
    var list = document.getElementById('stream-items-id').querySelectorAll('li[data-item-type="tweet"]');

    // Retweetは除く
    for(var i = 0; i < list.length; i++) {
      if(list[i].querySelector(':scope > div').getAttribute('data-screen-name').toLowerCase() === user_name.toLowerCase()) {
        arr.push(list[i].getAttribute('id'));
      }
    }
    return arr;

  }, USER_NAME);
});


// post内容の取得
casper.then(function() {/*
    dumpEcho(this, post_ids);
    this.exit();*/
  for(var i = 0; i < post_ids.length; i++) {
    var post = this.evaluate(function(id) {
      try {
        var checkPinned = document.getElementById(id).getElementsByClassName('user-pinned');
        if (checkPinned.length === 0) {
            var content = document.getElementById(id).querySelector('.content');

            // image
            var ima_tag = content.querySelector('.AdaptiveMedia-container');
            var ima = '';
            if(ima_tag !== null) {
                switch(ima_tag.querySelector(':scope > div').getAttribute('class')) {
                    case 'AdaptiveMedia-singlePhoto':
                        ima = ima_tag.querySelector('img').getAttribute('src');
                        break;
                    case 'AdaptiveMedia-doublePhoto':
                    case 'AdaptiveMedia-triplePhoto':
                    case 'AdaptiveMedia-quadPhoto':
                        ima = ima_tag.querySelector(':scope > div > div:nth-of-type(1) img').getAttribute('src');
                        break;
                    case 'AdaptiveMedia-video':
                        ima = getComputedStyle(ima_tag.querySelector('.PlayableMedia-player'), null)
                            .backgroundImage
                            .replace(/(url\()|\)/g, '');
                        break;
                    default:
                        break;
                }
            }

            return {
                'image'   : ima,
                'sentence': content.querySelector('.js-tweet-text-container > p').innerText,
                'likes'   : content.querySelector('.ProfileTweet-actionList .ProfileTweet-action--favorite .ProfileTweet-actionCountForPresentation').textContent,
                'retweets': content.querySelector('.ProfileTweet-actionList .ProfileTweet-action--retweet .ProfileTweet-actionCountForPresentation').textContent,
                'replies' : content.querySelector('.ProfileTweet-actionList .ProfileTweet-action--reply .ProfileTweet-actionCountForPresentation').textContent,
                'created' : Number(content.querySelector('.stream-item-header .time a span:nth-of-type(1)').getAttribute('data-time'))
            };
        } else {
            return null;
        }
      } catch(err) {
        casper.log(err.name + ':' + err.message + '\n' + err.stack,"info")
      }
    }, post_ids[i]);
    try {
        if (post !== '') {
            post['likes'] = convertNum(post['likes']);
            post['retweets'] = convertNum(post['retweets']);
            post['replies'] = convertNum(post['replies']);
            post['created'] = parseInt(post['created']);
            if ( LAST_POST >= post['created'] || NUM_POSTS_NEW <= data['posts'].length) {
                NEXT = false;
                break;
            }
            data['posts'].push(post);
        }
    } catch (err) {
        casper.log(err.name + ':' + err.message + '\n' + err.stack,"info");
    }
  }
});

// 取得した情報をjsonで形式で表示
casper.then(function() {
  if(Object.keys(data).length === 0 && data.constructor === Object) {
    dumpEcho(this, {'error': 'element wrong.'});
  }
  else {
      data['num_posts'] = data['posts'].length;
    dumpEcho(this, data);
  }
});


casper.run(function() {
   casper.exit();
});



function dateFormatter(date) {
  var y = date.getFullYear();
  var arr = [
    date.getMonth() + 1,
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds()
  ];

  for(var i = 0; i < arr.length; i++) {
    if(arr[i] < 10) {
      arr[i] = '0' + arr[i];
    }
  }

  return y + '-' + arr[0] + '-' + arr[1] + ' ' + arr[2] + ':' + arr[3] + ':' + arr[4];
}


/*
 * 数値変換
 */
function convertNum(str) {
  if(str === null || '') {
    return 0;
  }

  str = str.replace(/,/g, '');
  if(str.indexOf('M') != -1) {
    return Number(str.replace(/M/g, '')) * 1000000;
  } else if(str.indexOf('K') != -1) {
    return Number(str.replace(/K/g, '')) * 1000;
  }

  return Number(str);
}

function getCapture(casper) {
    var date = new Date().getTime().toString();
    var hash = Math.floor((Math.random() * ((9999 + 1) - 1000)) + 1000).toString();
    casper.capture(date + hash + '.jpg');
}

/*
 * 配列の表示
 */
function dumpEcho(casper, arr) {
   casper.echo(JSON.stringify(arr, null, '  '));
   casper.log(JSON.stringify(arr, null, '  '), 'info');
}

function getIdPosts(casper, numStart) {
    return casper.evaluate(function(user_name) {
        var arr = [];
        var list = document.getElementById('stream-items-id').querySelectorAll('li[data-item-type="tweet"]');

        // Retweetは除く
        for(var i = numStart; i < list.length; i++) {
            if(list[i].querySelector(':scope > div').getAttribute('data-screen-name').toLowerCase() === user_name.toLowerCase()) {
                arr.push(list[i].getAttribute('id'));
            }
        }
        return arr;

    }, USER_NAME);
}

function checkHasNewPosts(casper) {
    return casper.evaluate(function (NUM_POSTS_NEW, LAST_POST, USER_NAME) {
        // get List tweet
        var arr = [];
        var list = document.getElementById('stream-items-id').querySelectorAll('li[data-item-type="tweet"]');
        for(var i = 0; i < list.length; i++) {
            if(list[i].querySelector(':scope > div').getAttribute('data-screen-name').toLowerCase() === USER_NAME.toLowerCase()) {
                arr.push(list[i]);
            }
        }
        var checkPostsNum = NUM_POSTS_NEW > arr.length;

        var checkTime = arr[arr.length - 1].querySelector('._timestamp').getAttribute('data-time');
        checkTime = LAST_POST < checkTime;

        return { 'time': arr[arr.length - 1].querySelector('._timestamp').getAttribute('data-time'),'num': arr.length, 'check': checkTime && checkPostsNum};

    }, {NUM_POSTS_NEW: NUM_POSTS_NEW, LAST_POST: LAST_POST, USER_NAME: USER_NAME});
}

function loopScroll(casper) {
    var lengthCurrent = casper.evaluate(function () {
        return document.getElementById('stream-items-id').querySelectorAll('li[data-item-type="tweet"]').length;
    });
    casper.scrollToBottom();
    casper.waitFor(function check() {
        var lengthNew = this.evaluate(function () {
            return document.getElementById('stream-items-id').querySelectorAll('li[data-item-type="tweet"]').length;
        });
        return lengthNew > lengthCurrent;
    }, function then() {
        var check = checkHasNewPosts(casper);
        if(check.check) {
            loopScroll(casper);
        }
    }, function timeOut() {});
}