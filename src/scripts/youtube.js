var casper = require('casper').create({
    viewportSize: { width: 1024, height: 768 },
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36'
});


var arg_arr = casper.cli.args;
var BASE_URL = 'https://www.youtube.com';
var USER_NAME = casper.cli.get("id");
var LIST_LAST_POSTS = casper.cli.get("list_post_id").split(',');
var NUM_POSTS_NEW = casper.cli.get("num_posts");
var YOUTUBE_KIND_OF = casper.cli.get("type");
var CHECK_API = false;

var ip = casper.cli.get("ip");
var port = casper.cli.get("port");
var protocol = casper.cli.get("protocol");

if (ip !== null && port !== null && protocol !== null ) {
    phantom.setProxy(ip, port, protocol, '', '');
}

// get url api when instagram call api
casper.on("resource.received", function(resource){
    CHECK_API = resource.url.indexOf("pagead/id") !== -1;
});

casper.start(BASE_URL + '/' + YOUTUBE_KIND_OF + '/' + USER_NAME + '/about');


var data = {};
casper.then(function() {
  try{
    var elCheckUser = this.evaluate(function() {
        return document.querySelectorAll('.channel-empty-message').length;
    });
    if(elCheckUser > 0) {
        dumpEcho(this, {'error': { 'param': USER_NAME }});
        //this.exit();
    }
    else {
        data = this.evaluate(function() {
            var div = document.getElementById('browse-items-primary').getElementsByClassName('about-metadata-container')[0];
            var introduce = div.querySelector('.about-description pre');

            return {
                'cover'    : getComputedStyle(document.getElementById('c4-header-bg-container'), null).backgroundImage.replace(/(url\()|\)|(=.*)/g, ''),
                'avatar'   : document.getElementsByClassName('channel-header-profile-image')[0].getAttribute('src').replace(/\/s100-c-k-no-mo-rj-c0xffffff/g, ''),
                'name'     : document.querySelector('.branded-page-header-title a').textContent,
                'followers': div.querySelector('.about-stats span:nth-of-type(1) b').textContent,
                'introduce': introduce === null ? '' : introduce.textContent,
                'posts'    : []
            };
        });

        data['followers'] = convertNum(data['followers']);
        data['user_name'] = USER_NAME;
    }
  } catch(err) {
    casper.log(err.name + ':' + err.message + '\n' + err.stack,"info")
  }
});


// 動画一覧ページへ移動、各動画のidを取得
casper.thenOpen(BASE_URL + '/' + YOUTUBE_KIND_OF + '/' + USER_NAME + '/videos', function() {
    // wait api call finished
    this.waitFor(function check() {
        return CHECK_API;
    }, function then() {
        data['posts'] = getData(this, 0);
    }, function timeOut() {
        data['posts'] = getData(this, 0);
    });
});


casper.then(function () {
    // one click load more we will get 30 post more; skip 30 posts we get first time.
    var numRepeat = Math.ceil((NUM_POSTS_NEW - 30) / 30);
    numRepeat = numRepeat !== 0 ? numRepeat : 1;
    this.repeat(numRepeat, function () {
      try {
        if (data['posts'].next) {
            var checkBtn =this.evaluate(function () {
               return document.getElementsByClassName('load-more-button').length > 0;
            });
            if(checkBtn) {
                this.click('button.load-more-button');
                this.waitFor(function check() {
                    var lengthNew = this.evaluate(function () {
                        return document.getElementById('channels-browse-content-grid').querySelectorAll(':scope > li > div').length;
                    });
                    return lengthNew > data['posts'].data.length;
                }, function then() {
                    var numPostsCurrent = data['posts'].data.length;
                    var result = getData(this, numPostsCurrent);
                    data['posts'].data = data['posts'].data.concat(result.data);
                    data['posts'].next = result.next;
                }, function onTimeout() {});
            } else {
                var numPostsCurrent = data['posts'].data.length;
                var result = getData(this, numPostsCurrent);
                data['posts'].data = data['posts'].data.concat(result.data);
                data['posts'].next = result.next;
            }
        }
      } catch(err) {
        casper.log(err.name + ':' + err.message + '\n' + err.stack,"info")
      }
    });
});


// 取得した情報をjsonで形式で表示（制限がかかるので動画詳細はyoutube_movie.jsで取得）
casper.then(function() {
    if(Object.keys(data).length === 0 && data.constructor === Object) {
        dumpEcho(this, {'error': 'element wrong.'});
    }
    else {
        data['posts'] = data['posts'].data;
        dumpEcho(this, data);
    }
    //this.exit();
});


// スクレイピング開始
casper.run(function() {
  casper.exit();
});


/*
 * 数値変換
 */
function convertNum(str) {
    if(str.length === 0) {
        return 0;
    }

    str = str.replace(/,/g, '');
    return Number(str);
}


/*
 * 配列の表示
 */
function dumpEcho(casper, arr) {
   casper.echo(JSON.stringify(arr, null, '  '));
   casper.log(JSON.stringify(arr, null, '  '), 'info');
}

function getCapture(casper, numPostNext) {
    var date = new Date().getTime().toString();
    var hash = Math.floor((Math.random() * ((9999 + 1) - 1000)) + 1000).toString();
    casper.capture(date + hash + '.jpg');
}

function getData(casper, numPostNext) {
    return casper.evaluate(function(LIST_LAST_POSTS, NUM_POSTS_NEW, numPostNext) {
        var arr = [];
        var next = true;
        var list = document.getElementById('channels-browse-content-grid').querySelectorAll(':scope > li > div');
        for(var i = numPostNext; i < list.length; i++) {
            if (NUM_POSTS_NEW <= arr.length) {
                next = false;
                break;
            }
            if(next) {
                for (var j = 0; j < LIST_LAST_POSTS.length; j++) {
                    if (list[i].getAttribute('data-context-item-id') === LIST_LAST_POSTS[j]){
                        next = false;
                        break;
                    }
                }
                if (next) {
                    arr.push({ 'video_id': list[i].getAttribute('data-context-item-id') });
                }
            } else {
                break;
            }
        }
        return {'next': next,'data': arr};
    }, {LIST_LAST_POSTS: LIST_LAST_POSTS, NUM_POSTS_NEW: NUM_POSTS_NEW, numPostNext: numPostNext});
}
