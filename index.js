var runner = require('./src/runner');

// App entry Point
exports.handler = function (event, context) {
    console.log('Running index.handler');
    console.log('==================================================');
    console.log('event', event);
    console.log('==================================================');

    if (event.hasOwnProperty('body')){
        event = JSON.parse(event['body'])
        console.log('http event', event);
        var filename = event.name +'.js'; // file should be placed inside /src/scripts/
        runner(filename, event, function(err, data) {
            console.log('result' + data);
            var result1 = {
            'body': data,
            'statusCode': 200,
            'headers': {
               'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
                }
            };
            context.succeed(result1);

        });
    } else {
        var filename = event.name +'.js'; // file should be placed inside /src/scripts/

        console.log('==================================================');
        // Execute the casperJS script and exit.
         runner(filename, event, function(err, data) {
            console.log('result' + JSON.parse(data))
            context.succeed(JSON.parse(data));

        });
    }
}
