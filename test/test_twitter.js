var assert = require('assert');

describe('test_ip', function () {
    this.timeout(60000);
    it('Should create', function (testDone) {
        var context = { invokeid: 'invokeid', done: function (message) {testDone();}, succeed: function (message) {testDone();}};
        var lambda = require("../index.js");
        input = {
            "name":"twitter",
            "id": "jimmytran86",
            "num_posts": 50,
            "timestamp_last_post_in_db": 0
        };

        lambda.handler(input , context);
        assert(lambda);
    });
});

