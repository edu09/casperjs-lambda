# CasperJS AWS Lambda Template

A [CasperJS](http://casperjs.org/) node.js app for [Amazon Lambda](http://aws.amazon.com/lambda/).
Based on [node-lambda-template](https://github.com/rebelmail/node-lambda-template) using [node-lambda](https://github.com/rebelmail/node-lambda).
The app includes a [PhantomJS](http://phantomjs.org/) binary (i.e., in the `/bin/` directory named: `phantomjs`) compiled for AWS Linux (https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.8-linux-x86_64.tar.bz2).

**Note:** If you want to use different PhantomJS binary in your project, then you will be need to download a latest binary from [here](https://bitbucket.org/ariya/phantomjs/downloads/) and replace it with `/bin/phantomjs` (i.e., Make sure you keep the name as is for now).

## Table of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Understanding the Codebase structure](#understanding-the-codebase-structure)
* [Contributing](#contributing)
* [About Me](#about-me)
* [License](#license)

## Installation

Install dependencies using npm. It'll install the AWS SDK as well as PhantomJS on the development machine.

```shell
$ npm install # yarn
```

## Deploy to AWS lambda

1. Configure your AWS credentials on ~/.aws/credentials
2. Deploy using serverless

    sls deploy --stage stg

## Test

    npm test

## Understanding the Codebase structure

This describes the app directory structure & conventions.

```
.                               # project root directory
├── node_modules                # project dependencies directory
│   ├── .bin                    # node_modules bin directory
│   ├── casperjs                # casperjs executable
│   ├── phantomjs               # phantomjs executable
│   └── ...                     # etc
├── bin                         # bin directory
│   ├── phantomjs               # local phantomjs executable
│   └── ...                     # etc
├── src                         # source files
│   ├── scripts                 # directory contains all casperjs scrapping scripts.
│   │   ├── sample-script.js    # sample casperjs script
│   │   └── ...                 # etc
│   ├── runner.js               # dynamic casperjs script runner
│   ├── utils.js                # utility functions inside here.
│   └── ...                     # etc
├── test                        # directory contains test files
│   ├── basic.js                # sample basic test
│   └── ...
├── event.json                  # it is where you mock your event
├── index.js                    # app main entry point
├── package.json                # project details (i.e., version, author info, dependencies, etc.)
├── README.md                   # project documentation guide.
└── ...                         # etc
```
